from acp_times import open_time
import arrow

import nose    # Testing framework
import logging

def test_open_c60_b200():
    # test between 0 and 200
    # This is asserting that a control of 60km on a 200km brevet has an open time of  1hr 46m after start time
    # 60/34
    
    start_time = arrow.Arrow(2020,1,1)
    brevet_dist_km = 200
    control = 60
    
    assert(open_time(control, brevet_dist_km, arrow.get(start_time)) == (start_time.shift(hours=1,minutes=46)).isoformat())
    

def test_open_c320_b400():
    # test between 200 and 400
    # This is asserting that a control of 320km on a 400km brevet has an open time of  hr m after start time
    # 200/34 + 120/32
    
    start_time = arrow.Arrow(2020,1,1)
    brevet_dist_km = 400
    control = 320
    
    assert(open_time(control, brevet_dist_km, arrow.get(start_time)) == (start_time.shift(hours=9,minutes=38)).isoformat())
    
def test_open_c120_b200_wrong():
    # not equal test
    # This is asserting that a control of 120km on a 200km brevet does not have an open time of 4hr 32m after start time
    # 120/34

    start_time = arrow.Arrow(2020,1,1)
    brevet_dist_km = 200
    control = 120

    assert(open_time(control, brevet_dist_km, arrow.get(start_time)) != (start_time.shift(hours=4,minutes=32)).isoformat())
    
def test_open_c550_b600():
    # test between 400 and 600
    # This is asserting that a control of 550km on a 600km brevet has an open time of 17hr 8m   after start time
    # 200/34 + 200/32 + 150/30
    start_time = arrow.Arrow(2020,1,1)
    brevet_dist_km = 600
    control = 550

    assert(open_time(control, brevet_dist_km, arrow.get(start_time)) == (start_time.shift(hours=17,minutes=8)).isoformat())
    
def test_open_c799_b1000():
    # test between 600 and 1000
    # This is asserting that a control of 799km on a 800km brevet has an open time of 25hr 55m after start time
    # 200/34 + 200/32 + 200/30 + 199/28
    start_time = arrow.Arrow(2020,1,1)
    brevet_dist_km = 1000
    control = 799

    assert(open_time(control, brevet_dist_km, arrow.get(start_time)) == (start_time.shift(hours=25,minutes=55)).isoformat())


    
    
    
    