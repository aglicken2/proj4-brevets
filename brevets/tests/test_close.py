from acp_times import close_time
import arrow

import nose    # Testing framework
import logging

def test_close_c60_b200():
    # test between 0 and 600
    # This is asserting that a control of 60km on a 200km brevet has a close time of 4 hours after start time
    # 60/15 = 4
    
    start_time = arrow.Arrow(2020,1,1)
    brevet_dist_km = 200
    control = 60
    
    # close_time_test = close_time(control, brevet_dist_km, start_time)
    
    assert(close_time(control, brevet_dist_km, arrow.get(start_time)) == (start_time.shift(hours=4)).isoformat())

    
def test_close_c890_b1000_wrong():
    # a not equal test
    # This is asserting that a control of 890km on a 1000km brevet does NOT have a closing time of 36hr and 40min after start
    # 600/15 + 290/11.428 != 36hr 40min

    start_time = arrow.Arrow(2020,1,1)
    brevet_dist_km = 1000
    control = 890

    close_time_test = close_time(control,brevet_dist_km,start_time)

    assert(close_time(control, brevet_dist_km, arrow.get(start_time)) != (start_time.shift(hours=36,minutes=40)).isoformat())


def test_close_c890_b1000():
    # test between 600 and 1000
    # This is asserting that a control of 890km on a 1000km brevet has a closing time of 65hr and 23min after start
    # 600/15 + 290/11.428 = 65hr 23min

    start_time = arrow.Arrow(2020,1,1)
    brevet_dist_km = 1000
    control = 890

    # close_time_test = close_time(control, brevet_dist_km, start_time)

    assert(close_time(control, brevet_dist_km, arrow.get(start_time)) == (start_time.shift(hours=+65,minutes=+23)).isoformat())