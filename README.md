# Project 4: Brevet time calculator with Ajax

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code. Completed by Anne Glickenhaus aglicken@uoregon.edu.
This program calculates opening and closing times for control points along a specified length of brevet (up to 1000km long).


## ACP controle times

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders). The description is ambiguous, but the examples help.

We are essentially replacing the calculator here (https://rusa.org/octime_acp.html).

## AJAX and Flask reimplementation

The RUSA controle time calculator is a Perl script that takes an HTML form and emits a text page in the above link. 

This implementation fills in times as the input fields are filled using Ajax and Flask.

## Testing

A suite of nose test cases are run when the docker container is built.
